﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace PersonMatcher
{
    class ImportXML : ReadFile
    {

        private static readonly XmlSerializer XmlSerializer = new XmlSerializer(typeof(List<Person>));


        public ImportXML()
        {

        }


        public override void readfile(List<Person> list, string filename)
        {
            filename = AppendExtension(filename, "xml");
            StreamReader reader = new StreamReader(filename);
            List<Person> data = XmlSerializer.Deserialize(reader.BaseStream) as List<Person>;
            if (data != null)
            {
                foreach (Person thing in data)
                    list.Add(thing);
            }
        }
    }

}
