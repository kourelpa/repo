﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace PersonMatcher
{
    class ExportFile : WriteFile
    {

        public override void writeFile(PairCollection p, string filename)
        {
            if (p == null || p.getPairs() == null)
            {
                Console.WriteLine("No matches found");
                System.Environment.Exit(1);
            }

            filename = AppendExtension(filename, "txt");
            StreamWriter writer = new StreamWriter("../../" + filename);
            foreach (var pairs in p.getPairs())
            {
                writer.WriteLine("\n" + pairs.DisplayID());
            }
            writer.Close();
            Console.WriteLine("File was written");
        }
    }
}
