﻿using System.Runtime.Serialization;// for datacontract
using System;
namespace PersonMatcher
{// holds all the information relevant to a person entity. Contains functions only directly related to person attributes

    public class Person
    {
        
        public int ObjectId { get; set; }
        
        public String StateFileNumber { get; set; }

        public String SocialSecurityNumber { get; set; }

        public String FirstName { get; set; }

        public String MiddleName { get; set; }

        public String LastName { get; set; }

        public int BirthYear { get; set; }

        public int BirthMonth { get; set; }

        public int BirthDay { get; set; }

        public String Gender { get; set; }

        public String NewbornScreeningNumber { get; set; }

        public String IsPartOfMultipleBirth { get; set; }

        public int BirthOrder { get; set; }

        public String BirthCountry { get; set; }

        public String MotherFirstName { get; set; }

        public String MotherMiddleName { get; set; }

        public String MotherLastName { get; set; }

        public String Phone1 { get; set; }

        public String Phone2 { get; set; }

        public override string ToString()
        {// uses string interpolation for variable insertion into string.
            return $"Id={ObjectId}, FirstName={FirstName}, MiddleName={MiddleName}, LastName={LastName}, BirthYear={BirthYear}, BirthMonth={BirthMonth}, BirthDay={BirthDay}";
        }
    }
}
// I really wanted to make these members protected, but due to the inheiritance I would not have been able to create the objects in the program.cs scope with defined input for the person class.