﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher
{// basic function: hold and display pairs of Person
    public class Pair
    {
        private Person One;
        private Person Two;

        public Pair(Person o, Person t)
        {
            One = o;
            Two = t;
        }

        public string Display()
        {
            return "\n\t" + One.ToString() + "\n\t" + Two.ToString();
        }
        public string DisplayID() {
            return "\n" + One.ObjectId + "," + Two.ObjectId;
        }
    }
}
