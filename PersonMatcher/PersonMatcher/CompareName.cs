﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher 
{
    public class CompareName : Comparison
    {
        public override bool comparePerson(Person first, Person second)
        {
            // I am going to say they are a match if 2/3 names match. someone may put a nickname or abbreviated first name, also someone may or may not put a middle name, or their last name may change if married
            if (first == null || second == null)
                return false;
            int matchCount = 0;

            if ( !String.IsNullOrEmpty(first.FirstName) && !String.IsNullOrEmpty(second.FirstName))
                matchCount = (first.FirstName == second.FirstName) ? matchCount+1 : matchCount;// make unit test here
            if ( !String.IsNullOrEmpty(first.MiddleName) && !String.IsNullOrEmpty(second.MiddleName))
                matchCount = (first.MiddleName == second.MiddleName) ? matchCount+1 : matchCount;
            if ( !String.IsNullOrEmpty(first.LastName) && !String.IsNullOrEmpty(second.LastName))
                matchCount = (first.LastName == second.LastName) ? matchCount+1 : matchCount;

            if (matchCount >= 2)
                return true;

            return false;
        }
    }
}