﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PersonMatcher
{
    class ExportConsole : WriteFile
    {
        public override void writeFile(PairCollection p, string filename)
        {
            if (p == null || p.getPairs() == null)
            {
                Console.WriteLine("No matches were found, so no output can be written");
                System.Environment.Exit(1);
            }
            Console.WriteLine("\nMatches found for  "+ filename + ":\n");
            foreach (var pairs in p.getPairs())
            {
                Console.WriteLine("Match:\n" + pairs.Display());
            }
        }
    }
}
