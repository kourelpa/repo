﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher
{
    class Program
    {

            private const string DefaultInputFilename = "../../SampleData";
            private const string DefaultOutputFilename = "Person_Matches";

        //private static readonly ImporterExporter[] ImporterExporters = new ImporterExporter[]
        //                {
        //                    new JsonImporterExporter() { Name = "JSON", Description  = "JavaScript Object Notation"},
        //                    new XmlImporterExporter() { Name = "XML", Description = "Extensible Markup Language"}
        //                };

        private static readonly ReadFile[] InputFiles = new ReadFile[]
                 {
                            new ImportJSON() { Name = "JSON"},
                            new ImportXML() { Name = "XML"}
                 };

        private static readonly WriteFile[] OutputFiles = new WriteFile[]
                {
                            new ExportFile() { Name = "TEXTFILE"},
                            new ExportConsole() { Name = "CONSOLE"}
                            
                };

        private static readonly Comparison[] comparisonTypes = new Comparison[]
                {
                            new CompareMother() { Name = "MOTHERCOMPARISON" },
                            new CompareCredentials() { Name = "CREDENTIALCOMPARISON" },
                            new CompareName() { Name = "NAMECOMPARISON" }
                };

        public static void Main(string[] args)
        {
            ReadFile rf = RequestInputFormat();
            if (rf == null)
                return;
            WriteFile wf = RequestOutputFormat();
            if (wf == null)
                return;
            Comparison ct = RequestComparisonType();
            if (ct == null)
                return;
            string inputFilename = RequestinFileName();
            if (inputFilename == null)
                return;
            string outputFileName = RequestOutFileName();
            if (outputFileName == null)
                outputFileName = DefaultOutputFilename;

            // give the PersonCollection the input filename and the input file type
            PersonCollection newInput = new PersonCollection(rf, inputFilename);
            try
            {
                newInput.read();
            }
            catch
            {
                Console.WriteLine("That file name will not work.");
                System.Environment.Exit(1);
            }
            // give the personhandler the desired write type, comparison type, and the PersonCollection object
            PersonHandler personhandler = new PersonHandler(newInput, ct, wf);
            personhandler.OutputFileName = outputFileName;
            personhandler.findMatch();
            personhandler.write();

            

            Console.WriteLine("\nType ENTER to exit");
            Console.WriteLine("");
            Console.ReadLine();
        }


        private static ReadFile RequestInputFormat()
        {
            ReadFile result = null;
            while (result == null)
            {
                Console.WriteLine("File Format Types:");
                foreach (ReadFile item in InputFiles)
                    Console.WriteLine($"\t{item.Name.PadRight(10)}");
                Console.Write("Specify which format type you want to work or EXIT? ");
                string response = Console.ReadLine()?.Trim().ToUpper();

                if (response == "EXIT")
                    return null;

                foreach (ReadFile item in InputFiles)
                {
                    if (response == item.Name)
                    {
                        result = item;
                        break;
                    }
                }
            }

            return result;
        }

        private static WriteFile RequestOutputFormat()
        {
            WriteFile result = null;
            while (result == null)
            {
                Console.WriteLine("File Format Types:");
                foreach (WriteFile importerExporter in OutputFiles)
                    Console.WriteLine($"\t{importerExporter.Name.PadRight(10)}");
                Console.Write("Specify how you would like to display your results or EXIT? ");
                string response = Console.ReadLine()?.Trim().ToUpper();

                if (response == "EXIT")
                    return null;

                foreach (WriteFile item in OutputFiles)
                {
                    if (response == item.Name)
                    {
                        result = item;
                        break;
                    }
                }
            }

            return result;
        }

        private static Comparison RequestComparisonType()
        {
            Comparison result = null;
            while (result == null)
            {
                Console.WriteLine("File Format Types:");
                foreach (Comparison item in comparisonTypes)
                    Console.WriteLine($"\t{item.Name.PadRight(10)}");
                Console.Write("specify which type of comparison you would like to do or EXIT? ");
                string response = Console.ReadLine()?.Trim().ToUpper();

                if (response == "EXIT")
                    return null;

                foreach (Comparison item in comparisonTypes)
                {
                    if (response == item.Name)
                    {
                        result = item;
                        break;
                    }
                }
            }

            return result;
        }

        private static string RequestinFileName()
        {
            string result = null;
            Console.Write($"Enter input data file path or EXIT (default={DefaultInputFilename})? ");
            string response = Console.ReadLine()?.Trim();

            if (string.IsNullOrWhiteSpace(response))
                response = DefaultInputFilename;

            if (response != "EXIT")
                result = response;

            return result;
        }

        private static string RequestOutFileName()
        {
            string result = null;
            Console.Write($"Enter output data file name / title [optional] or EXIT (default={DefaultOutputFilename})? ");
            string response = Console.ReadLine()?.Trim();

            if (string.IsNullOrWhiteSpace(response))
                response = DefaultOutputFilename;

            if (response != "EXIT")
                result = response;

            return result;
        }
    }
    
}
