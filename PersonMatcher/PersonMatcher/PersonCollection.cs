﻿using System;
using System.Collections.Generic;

namespace PersonMatcher
{
    public class PersonCollection : List<Person>
    {
        private ReadFile readType;
        public string InputFileName { get; set; }

        public PersonCollection(ReadFile rf, string ifn)
        {
            readType = rf;
            InputFileName = ifn;
        }

        public void PrintCollection(string header)
        {
            Console.WriteLine("");
            Console.WriteLine($"Number of {header} back in: {Count}");

            foreach (Person p in this)
            {
                Console.WriteLine(p);
            }
        }

        public void read()
        {
            readType.readfile(this, InputFileName);
            

        }
    }
}
