﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher
{
    public abstract class Comparison
    {
        public string Name { get; set; }
        public abstract bool comparePerson(Person f, Person s);
    }
}
