﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher
{
    public class CompareMother : Comparison
    {
        public override bool comparePerson(Person first, Person second)
        {
            // I am going to say they are a match if 2/3 names match. someone may put a nickname or abbreviated first name, also someone may or may not put a middle name, or their last name may change if married
            if (first == null || second == null)
                return false;
            int matchCount = 0;
            if (!String.IsNullOrEmpty(first.MotherFirstName) && !String.IsNullOrEmpty(second.MotherFirstName))
                    matchCount = (first.MotherFirstName == second.MotherFirstName) ? matchCount+1 : matchCount;// make unit test here
            if ( !String.IsNullOrEmpty(first.MotherMiddleName) && !String.IsNullOrEmpty(second.MotherMiddleName))
                    matchCount = (first.MotherMiddleName == second.MotherMiddleName) ? matchCount+1 : matchCount;
            if ( !String.IsNullOrEmpty(first.MotherLastName) && !String.IsNullOrEmpty(second.MotherLastName))
                    matchCount = (first.MotherLastName == second.MotherLastName) ? matchCount+1 : matchCount;

            if (matchCount >= 2)
                return true;

            return false;
        }
    }

}
