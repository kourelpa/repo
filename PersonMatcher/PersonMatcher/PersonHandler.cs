﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher
{
    public class PersonHandler
    {
        
        private WriteFile writeType;
        private Comparison compareType;
        private Person First;
        private Person Second;
        private PersonCollection rawPeople;
        private PairCollection pairedPeople;


        public string OutputFileName { get; set; }


        // constructor takes in the dyamic runtime decisions for the application
        public PersonHandler(PersonCollection input, Comparison cType, WriteFile wType)
        {
            rawPeople = input;
            compareType = cType;
            writeType = wType;

        }



        public bool compare()
        {
            return compareType.comparePerson(First, Second);
        }

        public void findMatch()
        {
            pairedPeople = new PairCollection();
            foreach (Person p1 in rawPeople)
            {
                foreach (Person p2 in rawPeople)
                {
                    First = p1;
                    Second = p2;

                    if (First.ObjectId != Second.ObjectId && compare() == true)// if they are not the same object
                    {
                        pairedPeople.AddPair(new Pair(First, Second));
                    }
                }
            }
        }


        public void write()
        {
            writeType.writeFile(pairedPeople, OutputFileName);
        }

        //bool Comparison.comparePerson(Person f, Person s)
        //{
        //    throw new NotImplementedException();
        //}
    }
}


//TODO: ADD IN SOMEWHERE THE ACTUAL CHOICES OF WHICH CONCRETE METHODS TO USE.