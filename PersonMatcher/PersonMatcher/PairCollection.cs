﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher
{
    public class PairCollection
    {

        private List<Pair> collection;

        public PairCollection()
        {
            collection = new List<Pair>();
        }

        public void AddPair(Pair p)
        {
            collection.Add(p);
        }
        public List<Pair> getPairs()
        {
            return collection;
        }
    }
}
