﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMatcher
{
    public class CompareCredentials : Comparison
    {
        public override bool comparePerson(Person first, Person second)
        {
            // I am going to say they are a match if 2/3 names match. someone may put a nickname or abbreviated first name, also someone may or may not put a middle name, or their last name may change if married
            if (first == null || second == null)
                return false;

            int matchCount = 0;
            if (first.BirthDay >0 && second.BirthDay > 0)
                matchCount = (first.BirthDay == second.BirthDay) ? matchCount+1 : matchCount;// make unit test here

            if (first.BirthMonth > 0 && second.BirthMonth > 0)
                matchCount = (first.BirthMonth == second.BirthMonth) ? matchCount+1 : matchCount;

            if (first.BirthYear > 0 && second.BirthYear > 0)
                matchCount = (first.BirthYear == second.BirthYear) ? matchCount+1 : matchCount;

            if (first.SocialSecurityNumber != null && second.SocialSecurityNumber != null)
                matchCount = (first.SocialSecurityNumber == second.SocialSecurityNumber) ? matchCount+1 : matchCount;

            if (first.Gender != null && second.Gender != null)
                matchCount = (first.Gender == second.Gender) ? matchCount+1 : matchCount;

            if (matchCount == 5)
                return true;

            return false;
        }
    }

}
