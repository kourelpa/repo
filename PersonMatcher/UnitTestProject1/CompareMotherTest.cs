﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class CompareMotherTest
    {
        [TestMethod]
        public void Test_ComparePerson_Mother()
        {
            // Assemble

            // create test people. some partially match, some are incomplete
            PersonMatcher.Person p1 = new PersonMatcher.Person() { MotherFirstName = "Kate", MotherMiddleName = "Sue", MotherLastName = "Thomas" };
            PersonMatcher.Person p2 = new PersonMatcher.Person() { MotherFirstName = "Sue", MotherMiddleName = "Jan", MotherLastName = "Jardine" };
            PersonMatcher.Person p3 = new PersonMatcher.Person() { MotherFirstName = "Jan", MotherMiddleName = "Jill", MotherLastName = "Jardine" };
            PersonMatcher.Person p4 = new PersonMatcher.Person() { MotherFirstName = "Jan", MotherMiddleName = "Jill", MotherLastName = null };
            PersonMatcher.Person p5 = new PersonMatcher.Person() { MotherFirstName = "Sue", MotherMiddleName = "Jan", MotherLastName = "Jardine" };

            //Act

            PersonMatcher.Comparison compareMother = new PersonMatcher.CompareMother();

            //Assert
            Assert.IsTrue(compareMother.comparePerson(p3, p4));// 3 and 4 should still match on 2/3 even though last name is null on 4
            Assert.IsTrue(compareMother.comparePerson(p2, p5));// both 2 and 5 are the same, should match
            Assert.IsFalse(compareMother.comparePerson(p1, p2));// 1 and 2 are classically different
            Assert.IsFalse(compareMother.comparePerson(p2, p3));// 2 and 3 have some matching names
            


        }
    }
}
