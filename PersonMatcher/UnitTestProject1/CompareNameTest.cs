﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class CompareNameTest
    {
        [TestMethod]
        public void Test_ComparePerson_Name()
        {

            // create test people. some partially match, some are incomplete
            PersonMatcher.Person p1 = new PersonMatcher.Person() { FirstName = "John", MiddleName = "George", LastName = "Thomas" };
            PersonMatcher.Person p2 = new PersonMatcher.Person() { FirstName = "Ferris", MiddleName = "Donald", LastName = "Jardine" };
            PersonMatcher.Person p3 = new PersonMatcher.Person() { FirstName = "Cameron", MiddleName = "Quinton", LastName = "Campbell" };
            PersonMatcher.Person p4 = new PersonMatcher.Person() { FirstName = "Cameron", MiddleName = "Quinton", LastName = "Campbell" };
            PersonMatcher.Person p5 = new PersonMatcher.Person() { FirstName = "Cameron", MiddleName = "", LastName = "" };
            PersonMatcher.Person p6 = new PersonMatcher.Person() { FirstName = null, MiddleName = "", LastName = "Campbell" };
            PersonMatcher.Person p7 = new PersonMatcher.Person() { FirstName = null, MiddleName = null, LastName = "Campbell" };


            PersonMatcher.Comparison name = new PersonMatcher.CompareName();


            Assert.IsFalse(name.comparePerson(p1, p2));// classic mismatch, return false
            Assert.IsTrue(name.comparePerson(p3, p4));// both are the same, they should match 3/3
            Assert.IsFalse(name.comparePerson(p4, p5));// first name matches, but one has two empty strings, should reject at 1/3
            Assert.IsFalse(name.comparePerson(p4, p6));// full name compared with person that has null, empty, and matching name. reject at 1/3
            Assert.IsFalse(name.comparePerson(p6, p7));//last names match, the rest is null or unequal, should reject at 1/3




        }
    }
}
