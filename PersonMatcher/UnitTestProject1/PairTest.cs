﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class PairTest
    {
        [TestMethod]
        public void Test_Display()
        {
            PersonMatcher.Person p1 = new PersonMatcher.Person();
            p1.FirstName = "Joe";
            p1.MiddleName = "Harold";
            p1.LastName = "Smith";
            PersonMatcher.Person p2 = new PersonMatcher.Person();
            p2.FirstName = "Joe";
            p2.MiddleName = "Harold";
            p2.LastName = "Smith";

            PersonMatcher.Pair testpair = new PersonMatcher.Pair(p1, p2);

            Assert.AreEqual(testpair.Display(), "\n\t" + p1.ToString() + "\n\t" + p2.ToString());
        }

        [TestMethod]
        public void Test_DispalyID()
        {
            PersonMatcher.Person p1 = new PersonMatcher.Person();
            p1.FirstName = "Joe";
            p1.MiddleName = "Harold";
            p1.LastName = "Smith";
            PersonMatcher.Person p2 = new PersonMatcher.Person();
            p2.FirstName = "Joe";
            p2.MiddleName = "Harold";
            p2.LastName = "Smith";

            PersonMatcher.Pair testpair = new PersonMatcher.Pair(p1, p2);

            Assert.AreEqual(testpair.DisplayID(), "\n" + p1.ObjectId + "," + p2.ObjectId);
        }
    }
}
